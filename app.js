///////////////////////////////////////////////////////////////////////
//   Ejercicio 1 de construcción de aplicaciones REST Practitioner   //
///////////////////////////////////////////////////////////////////////
//     Fernando Boroukhovitch - BBVA Uruguay - Octubre de 2018       //
///////////////////////////////////////////////////////////////////////

//Definición de variables y constantes globales, y objetos requeridos
var express = require('express');
var bodyParser = require('body-parser');
var usersFile = require('./user.json');
var accountsFile = require('./account.json');
var app = express();
app.use(bodyParser.json());
var port = process.env.port || 3000;
const URI = '/api-uruguay/v1/';

  //////////////////////////////
 //  Consultas y ABM USERS   //
//////////////////////////////

//GET /users - Obtiene la totalidad de los usuarios existentes
app.get(URI + 'users',
      function(req, res) {
        console.log('Ejecutando petición GET /users.');
        if (usersFile.length > 0){
            res.status(200); //Respuesta OK - Se devuelve/n usuario/s
            res.send(usersFile);
        } else {
            res.status(404); //Usuarios no encontrados
            res.send({
              "msg":"No existen usuarios."
            });
        };
        console.log('Ejecución GET /users finalizada.');
});

//GET /users con ID - Obtiene un usuario específico
app.get(URI + 'users/:idUser',
      function(req, res) {
        console.log('Ejecutando petición GET /users con ID.');
        let valorId = req.params.idUser;
        if (usersFile[valorId - 1] != undefined) {
            res.status(200); //Respuesta OK - Se devuelve usuario
            res.send(usersFile[valorId - 1]);
        } else {
          res.status(404); //Usuario no encontrado
          res.send({
            "msg":"Usuario no encontrado."
          });
        };
        console.log('Ejecución GET /users con ID finalizada.');
});

//GET /users con Query String - Obtiene un usuario específico con un query
//*************************************************************************
//  NOTA: No respeta definición petición REST - Se crea solo para pruebas
//*************************************************************************
app.get(URI + 'qs_users',
    function(req, res){
      console.log('Ejecutando petición GET /qs_users con Query String.');
      console.log('Query String recibido:' + JSON.stringify(req.query));
      var encontrado = false;
      var mensajeRespuesta = {"msg":"Usuario no encontrado."};
      for(i = 0; i < usersFile.length && !encontrado; ++i){
        if (usersFile[i].first_name === req.query.qname && usersFile[i].last_name === req.query.qlast_name){
            encontrado = true;
            mensajeRespuesta = usersFile[i];
        };
      };
      var statusCode = encontrado ? 200 : 404;
      res.status(statusCode);
      res.send(mensajeRespuesta);
      console.log(mensajeRespuesta);
      console.log('Ejecución GET /qs_users con Query String finalizada.');
  });

//POST /users (datos recibidos en el Body) - Inserta un nuevo usuario
app.post(URI + 'users',
      function(req, res) {
        console.log('Ejecutando petición POST /users.');
        let nuevoIdUser = usersFile.length + 1;
        let nuevoUser = {
          "id":nuevoIdUser,
          "first_name":req.body.first_name,
          "last_name":req.body.last_name,
          "email":req.body.email,
          "password":req.body.password
        };
        usersFile.push(nuevoUser);
        res.send({
          "msg":"Nuevo usuario creado exitosamente.", nuevoUser
          });
        console.log('Nuevo usuario creado exitosamente.');
        console.log(nuevoUser);
        console.log('Ejecución POST /users finalizada.');
});

//POST /users (datos recibidos en el Header) - Inserta un nuevo usuario
//*************************************************************************
//  NOTA: No respeta definición petición REST - Se crea solo para pruebas
//*************************************************************************
app.post(URI + 'hdr_users',
      function(req, res) {
        console.log('Ejecutando petición POST /hdr_ users.');
        let nuevoIdUser = usersFile.length + 1;
        let nuevoUser = {
          "id":nuevoIdUser,
          "first_name":req.headers.first_name,
          "last_name":req.headers.last_name,
          'email':req.headers.email,
          "password":req.headers.password
          };
        usersFile.push(nuevoUser);
        res.send({
          "msg":"Nuevo usuario creado exitosamente.", nuevoUser
          });
        console.log('Nuevo usuario creado exitosamente.');
        console.log(nuevoUser);
        console.log('Ejecución POST /hdr_users finalizada.');
});

//PUT /users (datos recibidos en el Body) - Actualiza un usuario específico
app.put(URI + 'users/:idUser',
      function(req, res) {
      console.log('Ejecutando petición PUT /users.');
      let valorId = req.params.idUser;
      let existe = false;
      if (usersFile[valorId - 1] == undefined)
            console.log('Usuario con id = ' + valorId +' a actualizar no existe');
      else {
        //Se asignan los códigos y mensajes de error en base a un boolean (otra forma diferente a la utilizada en el GET con ID){
        existe = true;
        let updateUser = {
            "id" : valorId,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
            };
          usersFile[valorId - 1] = updateUser;
          console.log('Usuario con id = ' + valorId +' actualizado correctamente.');
        };
        //Se asignan los códigos y mensajes de error en base a un boolean (otra forma diferente a la utilizada en el GET con ID)
        let msgResponse = existe ? {"msg":"Usuario actualizado correctamente."} : {"msg":"Actualización fallida. El usuario no existe."};
        let statusCode = existe ? 200 : 404;
        res.status(statusCode);
        res.send(msgResponse);
        console.log('Ejecución PUT /users finalizada.');
});

//DELETE /users - Elimina un usuario específico
app.delete(URI + 'users/:idUser',
  function(req, res) {
    console.log('Ejecutando petición DELETE /users.');
    let valorId = req.params.idUser;
    let existe = false;
    if (usersFile[valorId - 1] == undefined)
        console.log('Usuario con id = ' + valorId +' a eliminar no existe');
    else {
        existe = true;
        usersFile.splice([valorId - 1], 1);
        console.log('Usuario con id = ' + valorId +' eliminado exitosamente.');
    };
    //Se asignan los códigos y mensajes de error en base a un boolean (otra forma diferente a la utilizada en el GET con ID)
    let msgResponse = existe ? {"msg":"Usuario eliminado exitosamente."} : {"msg":"El usuario a eliminar no existe."};
    let statusCode = existe ? 200 : 404;
    res.status(statusCode).send(msgResponse);
    console.log('Ejecución DELETE /users finalizada.');
});

  ////////////////////////////////
 //  Consultas y ABM ACCOUNTS  //
////////////////////////////////

//GET /accounts - Obtiene la totalidad de las cuentas existentes
app.get(URI + 'accounts',
      function(req, res) {
        console.log('Ejecutando petición GET /accounts.');
        if (accountsFile.length > 0){
            res.status(200); //Respuesta OK - Se devuelve/n cuenta/s
            res.send(accountsFile);
        } else {
            res.status(404); //Cuentas no encontradas
            res.send({
              "msg":"No existen cuentas."
            });
        };
        console.log('Ejecución GET /accounts finalizada.');
});

//GET /accounts con ID - Obtiene una cuenta específica
app.get(URI + 'accounts/:idAccount',
      function(req, res) {
        console.log('Ejecutando petición GET /accounts con ID.');
        let valorId = req.params.idAccount;
        if (accountsFile[valorId - 1] != undefined) {
            res.status(200); //Respuesta OK - Se devuelve cuenta
            res.send(accountsFile[valorId - 1]);
        } else {
          res.status(404); //Cuenta no encontrada
          res.send({
            "msg":"Cuenta no encontrada."
          });
        };
        console.log('Ejecución GET /accounts con ID finalizada.');
});

//GET /accounts con Query String - Obtiene una cuenta específica con un query
//*************************************************************************
//  NOTA: No respeta definición petición REST - Se crea solo para pruebas
//*************************************************************************
app.get(URI + 'qs_accounts',
    function(req, res){
      console.log('Ejecutando petición GET /qs_accounts con Query String.');
      console.log('Query String recibido:' + JSON.stringify(req.query));
      var encontrado = false;
      var mensajeRespuesta = {"msg":"Cuenta no encontrada."};
      for(i = 0; i < accountsFile.length && !encontrado; ++i){
        if (accountsFile[i].IBAN === req.query.qiban){
            encontrado = true;
            mensajeRespuesta = accountsFile[i];
        };
      };
      var statusCode = encontrado ? 200 : 404;
      res.status(statusCode);
      res.send(mensajeRespuesta);
      console.log(mensajeRespuesta);
      console.log('Ejecución GET /qs_accounts con Query String finalizada.');
  });

//POST /accounts (datos recibidos en el Body) - Inserta un nueva cuenta
app.post(URI + 'accounts',
      function(req, res) {
        console.log('Ejecutando petición POST /accounts.');
        let nuevoIdAccount = accountsFile.length + 1;
        let nuevaCuenta = {
          "id":nuevoIdAccount,
          "idUser":req.body.idUser,
          "IBAN":req.body.IBAN,
          "tipoCuenta":req.body.tipoCuenta,
          "moneda":req.body.moneda,
          "saldo":req.body.saldo
        };
        accountsFile.push(nuevaCuenta);
        res.send({
          "msg":"Nueva cuenta creada exitosamente.", nuevaCuenta
          });
        console.log('Nueva cuenta creada exitosamente.');
        console.log(nuevaCuenta);
        console.log('Ejecución POST /accounts finalizada.');
});

//POST /accounts (datos recibidos en el Header) - Inserta un nueva cuenta
//*************************************************************************
//  NOTA: No respeta definición petición REST - Se crea solo para pruebas
//*************************************************************************
app.post(URI + 'hdr_accounts',
      function(req, res) {
        console.log('Ejecutando petición POST /hdr_ accounts.');
        let nuevoIdCuenta = accountsFile.length + 1;
        let nuevaCuenta = {
          "id":nuevoIdCuenta,
          "idUser":req.headers.iduser,
          "IBAN":req.headers.iban,
          "tipoCuenta":req.headers.tipocuenta,
          "moneda":req.headers.moneda,
          "saldo":req.headers.saldo
          };
        accountsFile.push(nuevaCuenta);
        res.send({
          "msg":"Nueva cuenta creada exitosamente.", nuevaCuenta
          });
        console.log('Nueva cuenta creada exitosamente.');
        console.log(nuevaCuenta);
        console.log('Ejecución POST /hdr_accounts finalizada.');
});

//PUT /accounts (datos recibidos en el Body) - Actualiza una cuenta específica
app.put(URI + 'accounts/:idAccount',
      function(req, res) {
      console.log('Ejecutando petición PUT /accounts.');
      let valorId = req.params.idAccount;
      let existe = false;
      if (accountsFile[valorId - 1] == undefined)
            console.log('Cuenta con id = ' + valorId +' a actualizar no existe');
      else {
        //Se asignan los códigos y mensajes de error en base a un boolean (otra forma diferente a la utilizada en el GET con ID){
        existe = true;
        let updateAccount = {
            "id" : valorId,
            "idUser":req.body.idUser,
            "IBAN":req.body.IBAN,
            "tipoCuenta":req.body.tipoCuenta,
            "moneda":req.body.moneda,
            "saldo":req.body.saldo
            };
          accountsFile[valorId - 1] = updateAccount;
          console.log('Cuenta con id = ' + valorId +' actualizada correctamente.');
        };
        //Se asignan los códigos y mensajes de error en base a un boolean (otra forma diferente a la utilizada en el GET con ID)
        let msgResponse = existe ? {"msg":"Cuenta actualizada correctamente."} : {"msg":"Actualización fallida. La cuenta no existe."};
        let statusCode = existe ? 200 : 404;
        res.status(statusCode);
        res.send(msgResponse);
        console.log('Ejecución PUT /accounts finalizada.');
});

//DELETE /accounts - Elimina una cuenta específica
app.delete(URI + 'accounts/:idAccount',
  function(req, res) {
    console.log('Ejecutando petición DELETE /accounts.');
    let valorId = req.params.idAccount;
    let existe = false;
    if (accountsFile[valorId - 1] == undefined)
        console.log('Cuenta con id = ' + valorId +' a eliminar no existe');
    else {
        existe = true;
        accountsFile.splice([valorId - 1], 1);
        console.log('Cuenta con id = ' + valorId +' eliminada exitosamente.');
    };
    //Se asignan los códigos y mensajes de error en base a un boolean (otra forma diferente a la utilizada en el GET con ID)
    let msgResponse = existe ? {"msg":"Cuenta eliminada exitosamente."} : {"msg":"La cuenta a eliminar no existe."};
    let statusCode = existe ? 200 : 404;
    res.status(statusCode).send(msgResponse);
    console.log('Ejecución DELETE /accounts finalizada.');
});

//Habilita recepciòn de peticiones por puerto 3000
app.listen(port);
console.log('Escuchando en el puerto ' + port);
