///////////////////////////////////////////////////////////////////////
//        Construcción de aplicaciones REST Practitioner             //
///////////////////////////////////////////////////////////////////////
//     Fernando Boroukhovitch - BBVA Uruguay - Octubre de 2018       //
///////////////////////////////////////////////////////////////////////
//           Módulo USERS - Peticiones REST de Usuarios              //
///////////////////////////////////////////////////////////////////////

//Importación constantes globales
var connectParms = require('./params.js');
const port = connectParms.portUsers;
const baseMLabURL = connectParms.baseMLabURL.valor;
const apikeyMLab = connectParms.apikeyMLab.valor;
const URI = connectParms.URI;

//Definición de variables y objetos requeridos
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var clienteMlab = requestJSON.createClient(baseMLabURL);

app.use(bodyParser.json());

//Habilita recepciòn de peticiones por el puerto configurado en param.js
app.listen(port);
console.log('Servicio USERS:          Escuchando en el puerto ' + port);

//////////////////////////////
//  Consultas y ABM USERS   //
//////////////////////////////

//GET /users consumiendo API REST de mLab - Obtiene todos los usuarios existentes
app.get(URI + 'users',
  function(req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/users.');
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    clienteMlab.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": "Error al obtener los usuarios."
            };
            res.status(500); //Error interno del servidor
        } else {
            if(body.length > 0) {
                response = body;
            } else {
                response = {
                  "msg":"No existen usuarios."
                };
                res.status(404); //Usuarios no encontrados
            };
        };
        res.send(response); //Respuesta OK - Se devuelve/n usuario/s
      });
      console.log('Ejecución GET /users finalizada.');
  });

  
//GET /users con ID consumiendo API REST de mLab - Obtiene un usuario específico
app.get(URI + 'users/:idUser',
  function (req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/users/:id (con ID).');
    let idUser = req.params.idUser;
    console.log("Id usuario: " + idUser);
    var queryString = 'q={"id_user":' + idUser + '}&';
    var queryStrField = 'f={"_id":0}&';
    clienteMlab.get('usuario?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
          response = {
            "msg":"Error obteniendo usuario."
          };
          res.status(500); //Error interno del servidor
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg":"Usuario no encontrado."
            };
            res.status(404); //Usuario no encontrado
          };
        };
        res.send(response); //Respuesta OK - Se devuelve usuario
      });
      console.log('Ejecución GET /api-uruguay/v1/users/:idUser (con ID) finalizada.');
  });

//GET /contactos por ID de usuario consumiendo API REST de mLab - Obtiene todos los contactos de un usuario específico
app.get(URI + 'contactos/:idUser',
  function (req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/contactos/:id (con ID).');
    let idUser = req.params.idUser;
    console.log("Id usuario: " + idUser);
    var queryString = 'q={"id_user":' + idUser + '}&';
    var queryStrField = 'f={"_id":0}&';
    clienteMlab.get('contactosXusuario?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
          response = {
            "msg":"Error obteniendo contactos."
          };
          res.status(500); //Error interno del servidor
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg":"Contactos no encontrados."
            };
            res.status(404); //Contactos no encontrados
          };
        };
        res.send(response); //Respuesta OK - Se devuelve contactos
      });
      console.log('Ejecución GET /api-uruguay/v1/contactos/:idUser (con ID) finalizada.');
  });


app.get(URI + 'users',
  function(req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/users.');
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    clienteMlab.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": "Error al obtener los usuarios."
            };
            res.status(500); //Error interno del servidor
        } else {
            if(body.length > 0) {
                response = body;
            } else {
                response = {
                  "msg":"No existen usuarios."
                };
                res.status(404); //Usuarios no encontrados
            };
        };
        res.send(response); //Respuesta OK - Se devuelve/n usuario/s
      });
      console.log('Ejecución GET /users finalizada.');
  });

//GET /users con ID consumiendo API REST de mLab - Obtiene un usuario específico
app.get(URI + 'users/:idUser',
  function (req, res) {
    console.log('Ejecutando petición GET /api-uruguay/v1/users/:id (con ID).');
    let idUser = req.params.idUser;
    console.log("Id usuario: " + idUser);
    var queryString = 'q={"id_user":' + idUser + '}&';
    var queryStrField = 'f={"_id":0}&';
    clienteMlab.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
          response = {
            "msg":"Error obteniendo usuario."
          };
          res.status(500); //Error interno del servidor
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg":"Usuario no encontrado."
            };
            res.status(404); //Usuario no encontrado
          };
        };
        res.send(response); //Respuesta OK - Se devuelve usuario
      });
      console.log('Ejecución GET /api-uruguay/v1/users/:idUser (con ID) finalizada.');
  });


//POST /users mLab (datos recibidos en el Body) - Inserta un nuevo usuario,
//controlando previamente que el ID del usuario no exista en la base de datos, collection USER.
app.post(URI + 'users',
  function(req, res){
    console.log('Ejecutando petición POST /api-uruguay/v1/users.');
    let idUser = req.body.id_user;
    console.log("Id usuario a crear: " + req.body.id_user);
    var queryString = 'q={"id_user":' + idUser + '}&';
    var queryStrField = 'f={"_id":0}&';
    clienteMlab.get('user?' + queryString + queryStrField + apikeyMLab,
      function(error, respuestaMLab, body){
        if (error){
            res.status(500); //Error interno del servidor
            res.send({
              "msg":"Error verificando existencia de ID de usuario recibido."
            });
        } else {
          if(body.length > 0) {
            console.log('El usuario ya existe. Usuario no creado.');
            res.status(400); //Error en los datos de usuario proporcionados
            res.send({
              "msg":"El usuario con Id = " + idUser + " ya existe. No se ha creado el usuario."
            });
          } else {
            clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, req.body,
              function(errorP, respuestaMLab, bodyP){
                if (!errorP) {
                  console.log('Usuario creado exitosamente.');
                  res.status(200); //OK Alta de usuario realizada con éxito
                  res.send(bodyP);
                } else {
                  console.log('Error al crear el usuario.');
                  res.status(500); //Error de servidor al insertar nuevo usuario
                  res.send({"msg":"Error al crear el usuario."});
                };
              });
          };
        };
      });
      console.log('Ejecución POST /api-uruguay/v1/users finalizada.');
  });

//PUT /users (datos recibidos en el Body) - Actualiza un usuario específico
app.put(URI + "users/:idUser",
  function(req, res){
    console.log('Ejecutando petición PUT /api-uruguay/v1/users/:idUser.');
    let idUser = req.params.idUser;
    console.log("Id usuario a actualizar: " + idUser);
    var queryStringID = 'q={"id_user":' + idUser + '}&';
    clienteMlab.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        if (respuesta != undefined) {
            var actualizaUser = {
              'id_user':+idUser,
              'first_name':req.body.first_name,
              'last_name':req.body.last_name,
              'email':req.body.email,
              'password':req.body.password,
              'accounts':req.body.accounts
            };
            //Nota: Otra forma de realizar la petición PUT es la siguiente (por el _id interno):
            //clienteMlab.put("user/" + respuesta._id.$oid +'?'+ apikeyMLab, actualizaUser,
            clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikeyMLab, actualizaUser,
            function(error, respuestaMLab,body){
              if (!error) {
                  console.log('Usuario actualizado exitosamente.');
                  res.status(200); //Actualización exitosa
                  res.send({"msg":"Usuario actualizado exitosamente."});
              } else {
                  console.log('Error al actualizar el usuario.');
                  res.status(500); //Error de servidor al actualizar el usuario
                  res.send({"msg":"Error al actualizar el usuario."});
              };
            });
        } else {
            console.log('El Id de usuario a actualizar no existe.');
            res.status(404); //Id de Usuario no existente
            res.send({"msg":"El Id de usuario a actualizar no existe."});
        };
      });
      console.log('Ejecución PUT /api-uruguay/v1/users/:idUser finalizada.');
  });

//DELETE /users - Elimina un usuario específico
app.delete(URI + "users/:idUser",
  function(req, res){
    console.log('Ejecutando petición DELETE /api-uruguay/v1/users/:idUser.');
    let idUser = req.params.idUser;
    console.log("Id usuario a eliminar: " + idUser);
    var queryStringID = 'q={"id_user":' + idUser + '}&';
    clienteMlab.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        if (respuesta != undefined) {
          console.log('Usuario eliminado exitosamente.');
          clienteMlab.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(error, respuestaMLab,body){
              res.status(200); //Id de usuario eliminado correctamente
              res.send({"msg":"Usuario eliminado exitosamente."});
            });
        } else {
          console.log('El Id de usuario a eliminar no existe.');
          res.status(404); //Id de usuario no existente
          res.send({"msg":"El Id de usuario a eliminar no existe."});
        };
      });
      console.log('Ejecución DELETE /api-uruguay/v1/users/:idUser finalizada.');
  });
