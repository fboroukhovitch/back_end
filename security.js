///////////////////////////////////////////////////////////////////////
//        Construcción de aplicaciones REST Practitioner             //
///////////////////////////////////////////////////////////////////////
//     Fernando Boroukhovitch - BBVA Uruguay - Octubre de 2018       //
///////////////////////////////////////////////////////////////////////
//           Módulo SECURITY - Peticiones de Seguridad               //
///////////////////////////////////////////////////////////////////////

//Importación constantes globales
var connectParms = require('./params.js');
const port = connectParms.portSecurity;
const baseMLabURL = connectParms.baseMLabURL.valor;
const apikeyMLab = connectParms.apikeyMLab.valor;
const URI = connectParms.URI;

//Definición de variables y objetos requeridos
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var  clienteMlab = requestJSON.createClient(baseMLabURL);

app.use(bodyParser.json());

//Habilita recepciòn de peticiones por el puerto configurado en param.js
app.listen(port);
console.log('Servicio SECURITY:       Escuchando en el puerto ' + port);

  ////////////////////////////////////////////////////
 //   LOGIN, LOGOUT y CHANGE-PASSWORD de Usuario   //
////////////////////////////////////////////////////

//POST /login
app.post(URI + "login",
  function (req, res){
    console.log('Ejecutando petición POST /api-uruguay/v1/login.');
    var email = req.body.email;
    var password = req.body.password;
    var queryStringLogin = 'q={"email":"' + email + '"}&';
    clienteMlab.get('usuario?'+ queryStringLogin + apikeyMLab,
      function(error, respuestaMLab , body) {
        var respuesta = body[0];
        if(respuesta != undefined){
          if (respuesta.password == password) {
            console.log("Login Correcto");
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            clienteMlab.put('usuario?q={"id_user": ' + respuesta.id_user + '}&' + apikeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                res.status(200); //Autorizado Login correcto
                res.send(respuesta);
                console.log(respuesta);
              });
          } else {
            console.log("Contraseña incorrecta.");
            res.status(401); //No autorizado Login incorrecto
            res.send({"msg":"Contraseña incorrecta."});
          };
        } else {
          console.log("Email incorrecto.");
          res.status(401); //No autorizado Login incorrecto
          res.send({"msg":"Email incorrecto."});
        };
      });
      console.log('Ejecución POST /api-uruguay/v1/login finalizada.');;
  });

//POST /logout
app.post(URI + "logout",
  function (req, res){
    console.log('Ejecutando petición POST /api-uruguay/v1/logout.');
    var email = req.body.email;
    var queryStringLogout = 'q={"email":"' + email + '"}&';
    clienteMlab.get('usuario?'+ queryStringLogout + apikeyMLab,
    function(error, respuestaMLab, body) {
      var respuesta = body[0];
      if(respuesta != undefined){
          console.log("Logout correcto.");
          var session = {"logged":true};
          var logout = '{"$unset":' + JSON.stringify(session) + '}';
          clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                res.status(200); //Logout correcto
                res.send({"msg": "Logout correcto."});
              });
      } else {
        console.log("Error en logout.");
        res.status(401); //Logout incorrecto
        res.send({"msg":"Error en logout. No se pudo realizar el logout para el email " + email});
      };
    });
    console.log('Ejecución POST /api-uruguay/v1/logout finalizada.');;
  });

  //POST /change-password
  app.post(URI + "change-password",
    function (req, res){
      console.log('Ejecutando petición POST /api-uruguay/v1/change-password.');
      var email = req.body.email;
      var password = req.body.password;
      var newPassword = req.body.newPassword;
      var queryStringLogin = 'q={"email":"' + email + '"}&';
      clienteMlab.get('usuario?'+ queryStringLogin + apikeyMLab,
        function(error, respuestaMLab , body) {
          var respuesta = body[0];
          if(respuesta != undefined){
            if (respuesta.password == password) {
              console.log("Contraseña anterior correcta.");
              var nuevaPassword = {"password":newPassword};
              var cambioPassword = '{"$set":' + JSON.stringify(nuevaPassword) + '}';
              clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikeyMLab, JSON.parse(cambioPassword),
                function(errorP, respuestaMLabP, bodyP) {
                  res.status(200); //Password actualizado correctamente.
                  res.send({"msg":"Contraseña de usuario actualizada exitosamente."});
                });
            } else {
              console.log("Contraseña anterior incorrecta.");
              res.status(401); //Cambio de contraseña no autorizado
              res.send({"msg":"Contraseña anterior incorrecta. Contraseña no actualizada."});
            };
          } else {
            console.log("Email incorrecto.");
            res.status(401); //No autorizado Email incorrecto
            res.send({"msg":"Email incorrecto."});
          };
        });
        console.log('Ejecución POST /api-uruguay/v1/change-password finalizada.');;
    });
